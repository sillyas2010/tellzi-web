const dotEnv = require('dotenv').config(),
  {
    PHASE_DEVELOPMENT_SERVER,
    PHASE_PRODUCTION_BUILD,
    PHASE_PRODUCTION_SERVER,
  } = require('next/constants'),
  EN = require('../locales/en.json'),
  SP = require('../locales/sp.json');

if (dotEnv.error) {
  throw dotEnv.error;
};

const languagesMap = {
    'en': {
      title: 'English',
      content: EN,
    },
    'sp': {
      title: 'Spanish',
      content: SP,
    },
  },
  languageKeys = Object.keys(languagesMap),
  devApiRoot = 'http://localhost:3000/api',
  prodApiRoot = 'https://test.tellzi.org/api',
  externalApiRoot = 'https://api.ciencuentos.org/v1';

const commonSettings = {
    languages: languageKeys,
    languagesMap: languagesMap,
    defaultLanguage: languageKeys[0],
    devApiRoot,
    prodApiRoot,
    externalApiRoot,
  },
  clientSettings = {...dotEnv, ...commonSettings},
  serverSettings = {...dotEnv, ...commonSettings,
    sourseMapDevTool: 'cheap-module-eval-source-map',
  },
  getEnv = (phase) => {
    const isDev = phase === PHASE_DEVELOPMENT_SERVER;
    // when `next build` or `npm run build` is used
    const isProd = phase === PHASE_PRODUCTION_BUILD || phase === PHASE_PRODUCTION_SERVER;

    const env = isProd ? 'production' : 'development';

    return {
      isDev,
      isProd,
      env,
    };
  };

module.exports = {
  getEnv,
  clientSettings,
  serverSettings,
};
