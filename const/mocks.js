export const storyDetail = {
  'id': 40,
  'title': 'The Two Rabbits & the sneaky fox in the forest',
  'synopsis': 'One-year-old Palesa spends the night before her birthday wide-eyed with excitement. She can’t wait to find out what each of her friends will bring her. But what will be interesting.',
  'categories': [
    1,
    8,
  ],
  'levels': [
    2,
  ],
  'author': {
    'id': 32,
    'fullName': 'Sindiwe Magona',
    'firstName': 'Na',
    'lastName': 'Khamheuang',
  },
  'translator': {
    'id': 15,
    'fullName': 'Mosa Mahlaba',
    'firstName': 'Mosa',
    'lastName': 'Mahlaba',
  },
  'illustrator': {
    'id': 30,
    'fullName': 'Sinomonde Ngwane',
    'firstName': 'Nivong',
    'lastName': 'Sengsakoun',
  },
  'language': 'en',
  'publisher': {
    'id': 1,
    'name': 'Book Dash',
  },
  'cover': 'https://via.placeholder.com/544/bda98d.png',
  'font': 'Gandhi Serif',
  'pages': [
    {
      'text': 'In the clear sky, the sun is sad and alone.',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/701494c89f0a44f686c56274c84223f2.jpg',
      'order': 416,
    },
    {
      'text': 'He looks down and sees fish swimming happily in the water.',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/62ee37770a72405aacdb4d8a0f26ed5b.jpg',
      'order': 417,
    },
    {
      'text': 'Wanting to join them, he tells the fish, “How happy you all look! May I play with you?”',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/a0f7477734e642c884c3be3c1c1798be.jpg',
      'order': 418,
    },
    {
      'text': 'As he comes closer, the fish shout, “Please, don’t come closer. It’s too hot for us!”',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/9c740d66ba2c4c9bbd5efe0b8d5ef097.jpg',
      'order': 419,
    },
    {
      'text': '“I will cover myself with a cloth,” says the sun.',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/1c1f3cce999147e7be4b1f41ddcec06b.jpg',
      'order': 420,
    },
    {
      'text': 'But, the cloth catches fire!',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/c48b54b1a5fd4ead99f684c9c0f6da53.jpg',
      'order': 421,
    },
    {
      'text': '“Boohoo!” the sun begins to cry.\r\n\t\t\t\t\t\t\r\nA bird flying past asks, “Why are you crying?”',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/a534935c6c7c4a638627d83aed63f060.jpg',
      'order': 422,
    },
    {
      'text': '"I want to play with the fish. But, I am too hot and will burn them if I get too close."',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/788bcbd6f214439a9575e4ece9796bff.jpg',
      'order': 423,
    },
    {
      'text': '“Go in the evening when it is cool. They will not get burnt then,” says the bird.',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/32824209377c4ec3acb1e8a3395e0897.jpg',
      'order': 424,
    },
    {
      'text': 'That evening, the sun comes down to the river.\r\n\t\t\t\t\t\t\r\nSplashhh!\r\n\t\t\t\t\t\t\r\n“I can finally swim with the fish!',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/7b7f19fbd49c490c9c0c87b57cf1a756.jpg',
      'order': 425,
    },
    {
      'text': '“Now it’s not too hot for us,” say the fish happily.\r\n\r\nThe sun, not sad and lonely anymore, says, "Let\'s play together every evening!"',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/6395dcb9576144579fe576fc54aafca1.jpg',
      'order': 426,
    },
    {
      'text': '',
      'textPosition': 'bottom',
      'picture': 'https://api.ciencuentos.org/media/stories/pictures/the-sun-takes-a-bath/8a49596e23824c7bba89335f4d90e011.jpg',
      'order': 427,
    },
  ],
  'myRating': null,
  'createdAt': '2020-02-05T11:15:17.194000-03:00',
  'updatedAt': '2020-02-10T14:23:02.415000-03:00',
};
