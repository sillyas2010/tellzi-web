import {createProxyMiddleware} from 'http-proxy-middleware';
import getConfig from 'next/config';

const settings = getConfig(),
  {
    externalApiRoot,
  } = settings.publicRuntimeConfig,
  proxy = createProxyMiddleware({
    target: externalApiRoot,
    changeOrigin: true,
    pathRewrite: {
      '^/api': '',
    },
  });

export const config = {
  api: {
    bodyParser: false,
  },
};

// proxy for api requests
export default (req, res) => {
  return proxy(req, res);
};


