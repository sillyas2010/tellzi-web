import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import useI18n from '@hooks/use-i18n';
import {Button} from '@material-ui/core';
import getConfig from 'next/config';

const config = getConfig(),
  {
    languagesMap,
  } = config.publicRuntimeConfig;

export default function Index() {
  const i18n = useI18n(),
    switchLang = () => {
      if (i18n.activeLocale === 'en') {
        i18n.locale('sp');
      } else {
        i18n.locale('en');
      }
    },
    currentLang = languagesMap[i18n.activeLocale].title;

  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          {i18n.t('intro.text')}
        </Typography>
        {i18n.t('intro.welcome', {username: 'User'})}<br/><br/>
        {i18n.t('intro.currentLang', {lang: currentLang})}<br/><br/>
        <Button variant="outlined" onClick={switchLang}>
          {i18n.t('intro.switchLang')}
        </Button>
      </Box>
    </Container>
  );
};
