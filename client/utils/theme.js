import {createMuiTheme} from '@material-ui/core/styles';
import {red} from '@material-ui/core/colors';

// Create a theme instance.
const titleFont = ['Bree Serif', 'Noto Sans', 'sans-serif'],
  titleFontStr = titleFont.join(','),
  theme = createMuiTheme({
    palette: {
      primary: {
        main: '#FFE353',
      },
      secondary: {
        main: '#F5EEE0',
        light: '#F5EEE0',
        dark: '#CCA863',
      },
      error: {
        main: red.A400,
      },
      background: {
        default: '#fff',
        paper: '#F6F6F0',
        yellow: '#fee353',
      },
      text: {
        primary: '#323232',
        secondary: 'rgba(50,50,50,.5)',
        yellow: '#fee353',
      },
      divider: '#242F3F',
      blue: '#6291F2',
      blueMedium: '#94C9FF',
      blueLight: '#D3E4F6',
      yellow: '#6291F2',
      yellowMedium: '#94C9FF',
      yellowLight: '#D3E4F6',
    },
    typography: {
      fontFamily: [
        'Noto Sans',
        'Bree Serif',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        'Ubuntu',
        '"Helvetica Neue"',
        'sans-serif',
      ].join(','),
      h1: {
        fontFamily: titleFontStr,
      },
      h2: {
        fontFamily: titleFontStr,
      },
      h3: {
        fontFamily: titleFontStr,
      },
      button: {
        fontFamily: titleFontStr,
      },
    },
    shape: {
      borderRadius: 24,
    },
    overrides: {
      MuiButton: {
        contained: {
          boxShadow: 'none',
        },
        root: {
          borderRadius: 16,
        },
      },
    },
  });

export default theme;
