import {makeStyles} from '@material-ui/core/styles';

export const modifyStyles = (rawStyles, rawModifyStyles) => {
  const classes = makeStyles(rawStyles)(),
    modifyClasses = makeStyles(rawModifyStyles)(),
    resultClasses = {...classes};

  Object.keys(modifyClasses).forEach((classKey) => {
    if (resultClasses[classKey]) {
      resultClasses[classKey] += ` ${modifyClasses[classKey]}`;
    } else {
      resultClasses[classKey] = modifyClasses[classKey];
    }
  });

  return resultClasses;
};

export const getBookAuthorsStr = (book, separator) => (
  book && book.author ? (
    `Author ${(!!book.author && book.author.fullName) || '...'}${separator}` +
    `Published by ${(!!book.publisher && book.publisher.name) || '...'}${separator}` +
    `Illustrated by ${(!!book.illustrator && book.illustrator.fullName) || '...'}${separator}` +
    `Translated by ${(!!book.translator && book.translator.fullName) || '...'}`
  ): ''
);
