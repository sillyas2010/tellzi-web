import {PropTypes} from 'prop-types';

export const bookShape = PropTypes.shape({
  id: PropTypes.number,
  title: PropTypes.string,
  pages: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string,
    textPosition: PropTypes.oneOf([
      'top', 'bottom', 'left', 'right',
    ]),
    picture: PropTypes.string,
    order: PropTypes.number,
  })),
  cover: PropTypes.string,
  synopsis: PropTypes.string,
  levels: PropTypes.arrayOf(PropTypes.number),
});
