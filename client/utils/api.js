import {storyDetail} from '@const/mocks';

import getConfig from 'next/config';

import fetch from 'isomorphic-unfetch';

import {useRef} from 'react';

import useSWR from 'swr';

const config = getConfig(),
  {
    apiRoot,
    externalApiRoot,
    isDev,
  } = config.publicRuntimeConfig,
  defaultFetcher = (...args) => fetch(...args).then((res) => res.json());

function constructGetUrl(baseUrl, params) {
  const url = new URL(baseUrl);
  url.search = new URLSearchParams(params).toString();

  return url;
};

function useStickyResult(value) {
  const val = useRef();
  if (value !== undefined) val.current = value;
  return val.current;
}

export function generateDeviceId() {
  const navigatorInfo = window.navigator,
    screenInfo = window.screen;
  let uid = navigatorInfo.mimeTypes.length;
  uid += navigatorInfo.userAgent.replace(/\D+/g, '');
  uid += navigatorInfo.plugins.length;
  uid += screenInfo.height || '';
  uid += screenInfo.width || '';
  uid += screenInfo.pixelDepth || '';

  return uid;
}

export function useApi(url, filters = {}, options = {}, type = 'internal') {
  const currentApiRoot = type === 'external' ? externalApiRoot : apiRoot,
    path = url ? constructGetUrl(`${currentApiRoot}${url}`, filters) : null,
    {data, error} = useSWR(() => path, defaultFetcher, options),
    stickyData = useStickyResult(data),
    result = {
      data: options.stickyResult ? stickyData : data,
      error,
    };

  if (isDev) {
    if (url === '/stories/test/') {
      return {data: storyDetail};
    }
  }

  return result;
};

export async function fetchApi(url, paramsBody = {}, method = 'POST', type = 'internal') {
  const currentApi = type === 'external' ? externalApiRoot : apiRoot;
  let path = `${currentApi}${url}`,
    options = {method},
    data, error;

  if (method === 'GET') {
    path = constructGetUrl(path, paramsBody);
  } else {
    options = {
      ...options,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(paramsBody),
    };
  }

  try {
    const response = await fetch(path, options);
    data = await response.json();
  } catch (err) {
    error = err;
  }

  return {data, error};
};


