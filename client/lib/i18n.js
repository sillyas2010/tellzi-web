import {createContext, useState, useRef, useEffect} from 'react';
import rosetta from 'rosetta';
import getConfig from 'next/config';

const config = getConfig(),
  {
    defaultLanguage,
    languagesMap,
  } = config.publicRuntimeConfig;


const i18n = rosetta();

export const I18nContext = createContext();

// default language
i18n.locale(defaultLanguage);

export default function I18n({children}) {
  const [locale, setLocale] = useState(defaultLanguage),
    [activeDict, setActiveDict] = useState(languagesMap[defaultLanguage].content),
    activeLocaleRef = useRef(locale || defaultLanguage),
    [, setTick] = useState(0),
    firstRender = useRef(true);

  // for initial SSR render
  if (locale && firstRender.current === true) {
    firstRender.current = false;
    i18n.locale(locale);
    i18n.set(locale, activeDict);
  }

  useEffect(() => {
    if (locale) {
      i18n.locale(locale);
      i18n.set(locale, activeDict);
      activeLocaleRef.current = locale;
      // force rerender
      setTick((tick) => tick + 1);
    }
  }, [locale, activeDict]);

  const i18nWrapper = {
    activeLocale: activeLocaleRef.current,
    t: (...args) => i18n.t(...args),
    locale: (l) => {
      const currentLang = languagesMap[l];

      i18n.locale(l);
      activeLocaleRef.current = l;
      if (currentLang) {
        const dict = currentLang.content;

        i18n.set(l, dict);
        setLocale(l);
        setActiveDict(dict);
      } else {
        setTick((tick) => tick + 1);
      }
    },
  };

  return (
    <I18nContext.Provider value={i18nWrapper}>{children}</I18nContext.Provider>
  );
}
