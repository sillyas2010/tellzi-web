import React from 'react';
import {useStyles} from './LogOffDropdown.style';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuList from '@material-ui/core/MenuList';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import {Box, Typography} from '@material-ui/core';

export default function LogOffDropdown(props) {
  const classes = useStyles();
  const [option, setOption] = React.useState('');


  const handleChange = (event) => {
    setOption(event.target.value);
  };


  return (
    <FormControl variant="filled" className={classes.formControl} style={{zIndex: '10'}}>
      <InputLabel id="select-filled-label" className={classes.inputLabel} shrink={false}>
        <Typography variant='subtitle1' className={classes.filter_placeholder}>
          Hello, Cristina!
        </Typography>
      </InputLabel>
      <Select
        disableUnderline
        labelId="select-filled-label"
        id="select-filled"
        value={option}
        onChange={handleChange}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
          classes: {paper: classes.dropdownStyle},
        }}
        className={classes.dropdownFilter}
        style={{backgroundColor: '#F6F6F0'}}
      >
        <MenuItem value={'Sign out'} style={{backgroundColor: '#FFFFFF'}}>Sign out</MenuItem>
      </Select>
    </FormControl>
  );
}
