import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: 'fit-content',
    left: 'auto',
    position: 'absolute !important',
    '& .MuiInputLabel-formControl': {
      top: '-18px',
      paddingLeft: '25px',
      zIndex: '6',
    },
    '& .MuiFilledInput-input': {
      paddingTop: '10px',
      backgroundColor: 'transparent !important',
      color: 'transparent',
    },
    '& .MuiSelect-select:focus': {
      backgroundColor: '#F6F6F0',
    },
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      top: '15px',
      right: '30px',
    },
    [theme.breakpoints.up('lg')]: {
      top: '24px',
      right: '90px',
    },
  },

  dropdownStyle: {
    borderRadius: '16px',
    backgroundColor: theme.palette.background.default,
    width: '174px',
    height: '52px',
    minWidth: 'unset !important',
    left: 'auto !important',
    boxShadow: '0px 20px 40px rgba(0, 0, 0, 0.1)',
    [theme.breakpoints.up('sm')]: {
      right: '30px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '88px',
    },
    '& .MuiListItem-root.Mui-selected': {
      backgroundColor: 'transparent',
    },
    '& .MuiListItem-root:hover': {
      backgroundColor: 'transparent',
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },

  dropdownFilter: {
    zIndex: '5',
    borderRadius: '100px !important',
    height: '32px',
    width: '193px',
    paddingTop: '0',
    paddingBottom: '0',
  },
  inputLabel: {
    zIndex: '6',
  },
  filter_placeholder: {
    fontFamily: 'Circular Std, sans-serif',
    fontWeight: '500',
    fontSize: '14px',
    color: theme.palette.text.primary,
  },
}));
