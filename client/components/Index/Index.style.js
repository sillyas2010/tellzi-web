import {createStyles, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    ['@global']: {
      ['body, html']: {
        padding: 0,
        margin: 0,
        overflowX: 'hidden',
        width: '100%',
      },
      body: {
        height: 'auto',
      },
      html: {
        height: '100%',
      },
    },
  }),
);

export {
  useStyles,
};
