import React from 'react';
import {Container, Grid, Box} from '@material-ui/core';
import Header from '@/client/components/Header';
import Footer from '@components/Footer/Footer';
import {useStyles} from './Index.style';
import BookCardList from '@components/BookCardList/BookCardList';
import useI18n from '@hooks/use-i18n';

const Index = () => {
  const classes = useStyles(),
    i18n = useI18n(),
    homeApi = {
      url: '/home/',
      params: {},
    };

  return (
    <Grid display="flex">
      <Header/>
      <Container maxWidth="lg">
        <Box mt={5} mb={10}>
          <BookCardList
            title={i18n.t('homepage.mostRead')}
            booksApi={homeApi}
            resAttr={'popular'}
            needsLoadMore={true}
            limit={8}
            pageCount={12}
          />
        </Box>
      </Container>
      <Footer/>
    </Grid>
  );
};

export default Index;
