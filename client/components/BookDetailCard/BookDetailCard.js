import BookCard from '@components/BookCard/BookCard';

import useI18n from '@hooks/use-i18n';

import {SvgIcon} from '@material-ui/core';

import ReadIcon from '@public/svg/read.svg';

import {bookShape} from '@utils/shapes';

import PropTypes from 'prop-types';

import {useStyles, rawStyles} from './BookDetailCard.style';

const BookDetailCard = ({book, categories, onRead}) => {
  const i18n = useI18n(),
    classes = useStyles(),
    categoryNames = (categories.map((cat) => cat.name)).join(' · '),
    renderIcon = (<SvgIcon
      className={classes.actionIcon}
      component={ReadIcon} viewBox="0 0 38 28"
    />);

  return (
    <>
      <BookCard
        noFollow={true}
        title={book.title}
        description={categoryNames}
        image={book.cover}
        level={book.levels[0]}
        actionVariant={'contained'}
        actionIcon={renderIcon}
        actionTitle={i18n.t('bookDetail.actionRead')}
        onAction={onRead}
        classes={rawStyles}
      />
    </>
  );
};

BookDetailCard.propTypes = {
  book: bookShape,
  onAction: PropTypes.func,
  categories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })),
};

export default BookDetailCard;
