import {makeStyles} from '@material-ui/core/styles';

const xsGutter = 16,
  smGutter = 24,
  rawStyles = (theme) => ({
    root: {
      backgroundColor: theme.palette.secondary.light,
      borderRadius: theme.shape.borderRadius,
      [theme.breakpoints.down('sm')]: {
        marginLeft: -smGutter,
        width: `calc(100% + ${smGutter*2}px)`,
        borderRadius: 0,
        borderBottomRightRadius: theme.shape.borderRadius,
        borderBottomLeftRadius: theme.shape.borderRadius,
      },
      [theme.breakpoints.down('xs')]: {
        marginLeft: -xsGutter,
        width: `calc(100% + ${xsGutter*2}px)`,
      },
    },
    bookContentWrapper: {
      [theme.breakpoints.up('md')]: {
        padding: 0,
      },
    },
    bookContent: {
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    media: {
      borderRadius: theme.shape.borderRadius,
      [theme.breakpoints.down('sm')]: {
        borderRadius: 0,
        borderBottomRightRadius: theme.shape.borderRadius,
        borderBottomLeftRadius: theme.shape.borderRadius,
      },
    },
    bookBadge: {
      backgroundColor: theme.palette.secondary.light,
      borderTopLeftRadius: theme.shape.borderRadius,
    },
    bookActions: {
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    bookAction: {
      padding: '1.7rem',
    },
    actionIcon: {
      fontSize: '2.3rem',
      marginRight: 'auto',
    },
    actionTitle: {
      marginRight: 'auto',
      marginLeft: '-2.3rem',
    },
  }),
  useStyles = makeStyles(rawStyles);

export {
  rawStyles,
  useStyles,
};
