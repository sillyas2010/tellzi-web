import {createStyles, makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    loadMoreButton: {
      display: 'flex',
      margin: 'auto',
      height: 78,
      width: 292,
      border: 'none',
      borderRadius: '16px',
      background: '#FFE353',
      fontFamily: 'CircularStd',
      fontWeight: 500,
      fontSize: 20,
      alignItems: 'center',
      color: '#212121',
      padding: '0 40px 0 0',
      justifyContent: 'space-evenly',
      cursor: 'pointer',
      ['&, &:hover, &:focus, &:active']: {
        outline: 'none',
      },
    },
  }),
);

export {
  useStyles,
};
