import React from 'react';
import {useStyles} from './LoadMoreButton.style';

export default function LoadMoreButton() {
  const classes = useStyles();

  return (
    <button type="button" className={classes.loadMoreButton}>
      <img src="/svg/loadmore.svg" alt="" />
      Load more
    </button>
  );
};
