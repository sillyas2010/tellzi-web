import PropTypes from 'prop-types';
import {SvgIcon, Button} from '@material-ui/core';
import {useStyles} from './CloseButton.style';
import CloseIcon from '@public/svg/close.svg';

const CloseButton = ({
  isNaked = false, isSquare = false, radius,
  backgroundColor, size, onClick, href,
}) => {
  const classes = useStyles({isNaked, backgroundColor, radius, size, isSquare});

  return (
    <Button variant="contained" href={href} className={classes.buttonBox} onClick={onClick}>
      <SvgIcon component={CloseIcon} viewBox="4 4 35 35" className={classes.buttonIcon}/>
    </Button>
  );
};

CloseButton.propTypes = {
  href: PropTypes.string,
  isSquare: PropTypes.bool,
  isNaked: PropTypes.bool,
  backgroundColor: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  radius: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,
};

export default CloseButton;
