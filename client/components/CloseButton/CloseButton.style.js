import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

export const useStyles = makeStyles((theme) => ({
  buttonBox: {
    display: 'inline-flex',
    backgroundColor: ({backgroundColor, isNaked}) => isNaked ?
      'transparent' : (backgroundColor || theme.palette.secondary.light),
    padding: '.6rem',
    borderRadius: ({isSquare, radius}) => isSquare ? 0 : (radius || '50%'),
    cursor: 'pointer',
    minWidth: '0',
    ['&:hover']: {
      backgroundColor: ({backgroundColor, isNaked}) => isNaked ?
        'transparent' :
        darken(backgroundColor || theme.palette.secondary.light, .05),
    },
    ['& .MuiTouchRipple-child']: {
      backgroundColor: ({backgroundColor, isNaked}) => isNaked ?
        'transparent' :
        darken(backgroundColor || theme.palette.secondary.light, .3),
    },
  },
  buttonIcon: {
    fontSize: ({size}) => size || '2rem',
  },
}));
