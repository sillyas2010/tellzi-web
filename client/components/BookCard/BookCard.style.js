import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

const cardRadius = 4,
  badgeRadius = 16,
  rawStyles = (theme) => ({
    root: {
      borderRadius: 0,
      borderBottomRightRadius: cardRadius,
      borderBottomLeftRadius: cardRadius,
      boxShadow: 'none',
    },
    mediaWrapper: {
      position: 'relative',
    },
    mediaAction: {},
    media: {
      borderRadius: 0,
      borderBottomRightRadius: cardRadius,
      borderBottomLeftRadius: cardRadius,
    },
    bookBadge: {
      position: 'absolute',
      bottom: 0,
      right: 0,
      width: 53,
      height: 53,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: theme.palette.background.paper,
      borderTopLeftRadius: badgeRadius,
    },
    bookBadgeImg: {
      width: '40%',
    },
    bookContentWrapper: {},
    bookContent: {},
    bookTitle: {
      fontSize: '1.9rem',
      marginBottom: '.2rem',
    },
    bookActions: {
      margin: '.7rem',
    },
    bookAction: {
      backgroundColor: theme.palette.primary.main,
      width: '100%',
      ['&:hover']: {
        backgroundColor: darken(theme.palette.primary.main, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.primary.main, .3),
      },
    },
    actionTitle: {
      fontFamily: theme.typography.h1.fontFamily,
      fontSize: '1.15rem',
      textTransform: 'none',
    },
  }),
  useStyles = makeStyles(rawStyles);

export {
  rawStyles,
  useStyles,
};
