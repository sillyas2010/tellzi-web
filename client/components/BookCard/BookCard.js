import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import {rawStyles} from './BookCard.style';
import Link from '@components/Link/Link';
import {modifyStyles} from '@utils/index';

const BookCard = ({
  image, level, title, description,
  actionTitle, actionIcon, onAction,
  actionVariant, href = '#', onClick,
  classes: rawModifyStyles, noFollow,
}) => {
  const classes = modifyStyles(rawStyles, rawModifyStyles);

  return (
    <Card className={classes.root}>
      <Link href={noFollow ? void(0) : href} raw={!!noFollow} onClick={onClick} className={classes.mediaWrapper}>
        <CardActionArea className={classes.mediaAction}>
          <CardMedia
            className={classes.media}
            component="img"
            image={image}
            alt={`"${title}" cover`}
          />
          { !!level &&
            <div className={classes.bookBadge}>
              <img src={`/svg/lvl${level}.svg`}
                className={classes.bookBadgeImg}
                alt={`"${title}" book level - ${level}`}
              />
            </div>
          }
        </CardActionArea>
      </Link>
      <CardContent className={classes.bookContentWrapper}>
        <Link href={noFollow ? void(0) : href}
          raw={!!noFollow} onClick={onClick}
          className={classes.bookContent}
        >
          <Typography gutterBottom variant="h3" component="h1"
            color="textPrimary"
            className={classes.bookTitle}
          >
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p"
            className={classes.bookCategories}
          >
            {description}
          </Typography>
        </Link>
      </CardContent>
      { !!actionTitle && !!onAction &&
        <CardActions className={classes.bookActions}>
          <Button variant={actionVariant || 'text'} onClick={onAction} className={classes.bookAction}>
            {!!actionIcon &&
              actionIcon
            }
            <Typography className={classes.actionTitle} variant="h6" component="h4">{actionTitle}</Typography>
          </Button>
        </CardActions>
      }
    </Card>
  );
};

BookCard.propTypes = {
  image: PropTypes.string,
  href: PropTypes.string,
  level: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  title: PropTypes.string,
  description: PropTypes.string,
  actionVariant: PropTypes.string,
  actionTitle: PropTypes.string,
  actionIcon: PropTypes.object,
  onAction: PropTypes.func,
  onClick: PropTypes.func,
  classes: PropTypes.func,
  noFollow: PropTypes.bool,
};

export default BookCard;
