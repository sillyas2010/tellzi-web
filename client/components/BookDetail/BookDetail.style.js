import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

const buttonGutter = 1,
  useStyles = makeStyles((theme) => ({
    closeButtonWrapper: {
      position: 'relative',
      zIndex: 10,
      [theme.breakpoints.down('sm')]: {
        position: 'absolute',
        left: 6,
        top: 22,
      },
      [theme.breakpoints.up('md')]: {
        marginTop: '2rem',
        marginBottom: '1.5rem',
      },
    },
    actionButton: {
      fontSize: '.85rem',
      backgroundColor: theme.palette.secondary.light,
      textTransform: 'capitalize',
      width: `calc( 50% - ${buttonGutter}rem )`,
      marginRight: `${buttonGutter}rem`,
      padding: '.7rem',
      ['&:hover']: {
        backgroundColor: darken(theme.palette.secondary.light, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.secondary.light, .3),
      },
      [theme.breakpoints.down('sm')]: {
        width: `calc( 50% - ${buttonGutter/2}rem )`,
      },
    },
    actionButtonWoMargin: {
      marginRight: 0,
    },
    actionIcon: {
      fontSize: '1rem',
      marginTop: '.3rem',
    },
    readButton: {
      marginTop: `${buttonGutter}rem`,
      padding: '1.3rem',
      backgroundColor: theme.palette.primary.main,
      width: '100%',
      ['&:hover']: {
        backgroundColor: darken(theme.palette.primary.main, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.primary.main, .3),
      },
    },
    readIcon: {
      fontSize: '2.3rem',
      marginRight: 'auto',
    },
    readTitle: {
      marginRight: 'auto',
      marginLeft: '-2.3rem',
      fontFamily: theme.typography.h1.fontFamily,
      fontSize: '1.15rem',
      textTransform: 'none',
    },
  }));

export {
  useStyles,
};
