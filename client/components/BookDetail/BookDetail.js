import BookDetailInfo from '@/client/components/BookDetailInfo/BookDetailInfo';

import BookDetailCard from '@components/BookDetailCard/BookDetailCard';
import BookCardList from '@components/BookCardList/BookCardList';
import CloseButton from '@components/CloseButton/CloseButton';
import BookReader from '@components/BookReader/BookReader';
import Footer from '@components/Footer/Footer';

import useI18n from '@hooks/use-i18n';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import {Box} from '@material-ui/core';

import {useApi} from '@utils/api';

import {useRouter} from 'next/router';

import {useState} from 'react';

import {useStyles} from './BookDetail.style';

const BookDetail = () => {
  const [isReading, setReading] = useState(false),
    openReader = () => setReading(true),
    closeReader = () => setReading(false),
    router = useRouter(),
    i18n = useI18n(),
    classes = useStyles(),
    {id} = router.query,
    {data: book, error} = useApi(`/stories/${id}/`),
    {data: categories} = useApi('/categories/'),
    bookCategories = book && book.categories && categories ?
      categories.results.filter((category) => ~book.categories.indexOf(category.id)) : [],
    similarCategory = bookCategories && bookCategories[0] && bookCategories[0].id,
    similarApi = {
      url: similarCategory ? '/stories/' : null,
      params: {categories: similarCategory},
    };

  return (
    <>
      <Container maxWidth="lg">
        <Box className={classes.closeButtonWrapper}>
          <CloseButton href="/"/>
        </Box>
        { book && book.title && categories ? (
          <>
            <Grid container justify="center">
              <Grid container item xs={12} md={11} lg={9}>
                <Grid item xs={12} md={6}>
                  <BookDetailCard book={book} onRead={openReader} categories={bookCategories}/>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box ml={3} mt={4} mr={5}>
                    <BookDetailInfo book={book} onRead={openReader} categories={bookCategories}/>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
            <Box mt={5} mb={10}>
              <BookCardList
                title={i18n.t('bookDetail.moreLikeThis')}
                booksApi={similarApi}
                needsLoadMore={true}
                limit={12}
                pageCount={12}
              />
            </Box>
          </>
        ) :
          (!!error ? <div>{error}</div> : 'loading book...')
        }
      </Container>
      <BookReader book={book} open={isReading} onClose={closeReader}/>
      <Footer/>
    </>
  );
};

export default BookDetail;
