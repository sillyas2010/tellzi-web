import BookReaderNav from '@components/BookReaderNav/BookReaderNav';

import {Modal, Container, Box, Grid, Typography, CardMedia} from '@material-ui/core';

import {bookShape} from '@utils/shapes';

import PropTypes from 'prop-types';

import {useState} from 'react';

import {useStyles} from './BookReader.style';


const BookReaderContent = ({book, currentPage, fontSizeMulti}) => {
  const classes = useStyles({fontSizeMulti}),
    page = book.pages[currentPage];

  return (
    <Container maxWidth="lg">
      <Box className={classes.modalContentWrapper}>
        <Grid container>
          <Grid container item xs={12} md={11}>
            <Grid item xs={12} md={8}>
              <Box className={classes.readerImageWrapper}>
                <CardMedia
                  className={classes.readerImage}
                  component="img"
                  image={page.picture}
                  alt={`"${book.title}" cover`}
                />
              </Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Box className={classes.readerTextWrapper}>
                <Typography className={classes.readerTitle} variant="h3" component="h2">
                  {book.title}
                </Typography>
                <Typography className={classes.readerText} variant="h5" component="p">
                  {page.text}
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

BookReaderContent.propTypes = {
  book: bookShape,
  currentPage: PropTypes.number,
  fontSizeMulti: PropTypes.number,
};

const BookReader = ({book, open, onClose}) => {
  const classes = useStyles(),
    [currentPage, setPage] = useState(0),
    [currentMark, setMark] = useState(0),
    [fontSizeMulti, setFontSizeMulti] = useState(1),
    changeFontSize = (e, value) => {
      setFontSizeMulti(value);
    },
    isMarkPage = !!book && !!book.pages && currentPage === book.pages.length;

  return (
    <Modal
      open={!!book && open}
      onClose={onClose}
    >
      <Box className={classes.modalWrapper + ' ' + (currentMark && classes.modalThankYouWrapper || '')}>
        { !isMarkPage &&
          <BookReaderContent currentPage={currentPage} book={book} fontSizeMulti={fontSizeMulti}/>
        }
        <BookReaderNav currentPage={currentPage}
          onClose={onClose} setPage={setPage} book={book}
          isMarkPage={isMarkPage} currentMark={currentMark}
          fontSizeMulti={fontSizeMulti} changeFontSize={changeFontSize}
          setMark={setMark}
        />
      </Box>
    </Modal>
  );
};

BookReader.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  book: bookShape,
};

export default BookReader;
