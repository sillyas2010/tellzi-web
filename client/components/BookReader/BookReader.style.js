import {makeStyles} from '@material-ui/core/styles';

const xsGutter = 16,
  smGutter = 24,
  marginTopVh = 15.5,
  useStyles = makeStyles((theme) => ({
    modalWrapper: {
      width: '100%',
      height: '100%',
      backgroundColor: theme.palette.common.white,
      ['&:focus']: {
        outline: 'none',
      },
      [theme.breakpoints.down('sm')]: {
        overflow: 'auto',
      },
    },
    modalThankYouWrapper: {
      width: 'auto',
      height: 'auto',
      minWidth: 343,
      padding: '4rem 0',
      textAlign: 'center',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      borderRadius: theme.shape.borderRadius,
    },
    modalContentWrapper: {
      display: 'inline-block',
      [theme.breakpoints.up('md')]: {
        marginTop: `${marginTopVh}vh`,
      },
    },
    readerImageWrapper: {
      overflow: 'hidden',
      borderBottomLeftRadius: theme.shape.borderRadius,
      borderBottomRightRadius: theme.shape.borderRadius,
      [theme.breakpoints.down('sm')]: {
        width: `calc(100% + ${smGutter*2}px)`,
        marginLeft: -smGutter,
      },
      [theme.breakpoints.down('xs')]: {
        width: `calc(100% + ${xsGutter*2}px)`,
        marginLeft: -xsGutter,
      },
      [theme.breakpoints.up('md')]: {
        marginRight: theme.spacing(3),
        maxHeight: `${100 - (marginTopVh * 2)}vh`,
        borderRadius: theme.shape.borderRadius,
      },
    },
    readerImage: {
      maxHeight: '100%',
    },
    readerTextWrapper: {
      paddingTop: theme.spacing(4),
      paddingBottom: '10.5rem',
      [theme.breakpoints.up('md')]: {
        paddingTop: theme.spacing(6),
        marginLeft: theme.spacing(3),
        maxHeight: `${100 - (marginTopVh)}vh`,
        overflow: 'auto',
      },
    },
    readerTitle: {
      fontSize: ({fontSizeMulti}) => `calc(1.8rem * ${fontSizeMulti}`,
      marginBottom: '.75rem',
    },
    readerText: {
      fontSize: ({fontSizeMulti}) => `calc(1.4rem * ${fontSizeMulti}`,
    },
  }));

export {
  useStyles,
};
