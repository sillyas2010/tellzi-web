import Link from '@components/Link/Link';
import CloseButton from '@components/CloseButton/CloseButton';

import useI18n from '@hooks/use-i18n';

import {Button, Modal, Typography, SvgIcon, Hidden} from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Divider from '@material-ui/core/Divider';

import PdfIcon from '@public/svg/pdf.svg';
import EpubIcon from '@public/svg/epub.svg';
import WhatsAppIcon from '@public/svg/whats-app.svg';
import LinkIcon from '@public/svg/link.svg';
import DownloadIcon from '@public/svg/download.svg';
import ShareIcon from '@public/svg/share.svg';
import ReadIcon from '@public/svg/read.svg';

import {bookShape} from '@utils/shapes';

import React from 'react';

import PropTypes from 'prop-types';

import {useStyles} from './BookActions.style';


const iconViewBox = '0 0 30 33',
  modalContext = {
    downloadContext: (book, i18n) => ({
      header: i18n.t('bookDetail.actionDownloadHeader'),
      actions: [
        {
          text: i18n.t('bookDetail.actionDownloadPdf'),
          iconProps: {viewBox: iconViewBox, component: PdfIcon},
          href: `/books/${book.id}/download/pdf`,
          onClick: () => {},
        },
        {
          text: i18n.t('bookDetail.actionDownloadEpub'),
          iconProps: {viewBox: iconViewBox, component: EpubIcon},
          href: `/books/${book.id}/download/epub`,
          onClick: () => {},
        },
      ],
    }),
    shareContext: (book, i18n) => ({
      header: i18n.t('bookDetail.actionShareHeader'),
      actions: [
        {
          text: i18n.t('bookDetail.actionShareWhatsApp'),
          iconProps: {viewBox: iconViewBox, component: WhatsAppIcon},
          href: '#',
          onClick: () => {},
        },
        {
          text: i18n.t('bookDetail.actionShareLink'),
          iconProps: {viewBox: iconViewBox, component: LinkIcon},
          href: '#',
          onClick: () => {},
        },
      ],
    }),
  };

const renderModalContent = (modalContext, classes, handleClose) => {
  return (
    <div className={classes.modalContent}>
      <div className={classes.modalHeader}>
        <div className={classes.modalClose}>
          <CloseButton onClick={handleClose} radius={16} isNaked={true}/>
        </div>
        <Typography variant="h3" className={classes.modalTitle}>
          {modalContext.header}
        </Typography>
      </div>
      <div className={classes.modalActions}>
        <List component="nav" className={classes.modalActionList}>
          {modalContext.actions.map((action) => (
            <div key={action.text}>
              <Divider light/>
              <ListItem href={action.href} button
                component={Link}
                className={classes.modalActionListItem}
                onClick={action.onClick}>
                <ListItemIcon className={classes.modalActionIcon}>
                  <SvgIcon className={classes.modalActionIconSvg} {...action.iconProps}/>
                </ListItemIcon>
                <ListItemText
                  primaryTypographyProps={{
                    color: 'textPrimary',
                    className: classes.modalActionText,
                  }}
                  primary={action.text}
                />
              </ListItem>
            </div>
          ))}
        </List>
      </div>
    </div>
  );
};

const BookActions = ({book, onRead}) => {
  const i18n = useI18n(),
    classes = useStyles(),
    [open, setOpen] = React.useState(false),
    [currentContext, setContext] = React.useState('downloadContext'),
    handleOpen = () => {
      setOpen(true);
    },
    handleClose = () => {
      setOpen(false);
    },
    handleOpenWithContext = (contextKey) => {
      return function() {
        setContext(contextKey);
        handleOpen();
      };
    };

  return (
    <>
      <Button
        variant="contained"
        onClick={handleOpenWithContext('downloadContext')}
        className={classes.actionButton}
        startIcon={<SvgIcon className={classes.actionIcon} component={DownloadIcon}/>}
      >
        {i18n.t('bookDetail.actionDownload')}
      </Button>
      <Button
        variant="contained"
        onClick={handleOpenWithContext('shareContext')}
        className={classes.actionButton + ` ${classes.actionButtonWoMargin}`}
        startIcon={<SvgIcon className={classes.actionIcon} component={ShareIcon}/>}
      >
        {i18n.t('bookDetail.actionShare')}
      </Button>
      <Hidden mdUp>
        <Button variant="contained"
          onClick={onRead}
          className={classes.readButton}
        >
          <SvgIcon
            className={classes.readIcon}
            component={ReadIcon} viewBox="0 0 38 28"
          />
          <Typography className={classes.readTitle} variant="h6" component="h4">
            {i18n.t('bookDetail.actionRead')}
          </Typography>
        </Button>
      </Hidden>
      <Modal
        open={open}
        onClose={handleClose}
      >
        {renderModalContent(
          modalContext[currentContext](book, i18n),
          classes,
          handleClose,
        )}
      </Modal>
    </>
  );
};

BookActions.propTypes = {
  book: bookShape,
  onRead: PropTypes.func,
};

export default BookActions;
