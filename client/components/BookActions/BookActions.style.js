import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

const buttonGutter = 1,
  useStyles = makeStyles((theme) => ({
    actionButton: {
      fontSize: '.85rem',
      backgroundColor: theme.palette.secondary.light,
      textTransform: 'capitalize',
      width: `calc( 50% - ${buttonGutter}rem )`,
      marginRight: `${buttonGutter}rem`,
      padding: '.7rem',
      ['&:hover']: {
        backgroundColor: darken(theme.palette.secondary.light, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.secondary.light, .3),
      },
      [theme.breakpoints.down('sm')]: {
        width: `calc( 50% - ${buttonGutter/2}rem )`,
      },
    },
    actionButtonWoMargin: {
      marginRight: 0,
    },
    actionIcon: {
      fontSize: '1rem',
      marginTop: '.3rem',
    },
    readButton: {
      marginTop: `${buttonGutter}rem`,
      padding: '1.3rem',
      backgroundColor: theme.palette.primary.main,
      width: '100%',
      ['&:hover']: {
        backgroundColor: darken(theme.palette.primary.main, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.primary.main, .3),
      },
    },
    readIcon: {
      fontSize: '2.3rem',
      marginRight: 'auto',
    },
    readTitle: {
      marginRight: 'auto',
      marginLeft: '-2.3rem',
      fontFamily: theme.typography.h1.fontFamily,
      fontSize: '1.15rem',
      textTransform: 'none',
    },
    modalContent: {
      display: 'inline-block',
      backgroundColor: theme.palette.common.white,
      borderRadius: theme.shape.borderRadius,
      position: 'absolute',
      minWidth: '21.5rem',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      ['&:hover, &:focus']: {
        outline: 'none',
      },
    },
    modalHeader: {
      minHeight: '2rem',
      lineHeight: '3.4rem',
      padding: '.5rem',
      textAlign: 'center',
    },
    modalTitle: {
      fontSize: '1rem',
      display: 'inline-block',
    },
    modalClose: {
      display: 'inline-block',
      position: 'absolute',
      left: '.5rem',
      top: '.5rem',
    },
    modalActionList: {
      padding: 0,
    },
    modalActionListItem: {
      padding: '1.2rem 1.3rem',
    },
    modalActionText: {
      fontWeight: 600,
      fontSize: '1.17rem',
    },
    modalActionIconSvg: {
      fontSize: '2rem',
    },
  }));

export {
  useStyles,
};
