import React, {useEffect, useState} from 'react';
import {useStyles, HeaderContainer} from './Header.style';
import HeaderImages from '../HeaderImages/HeaderImages';
import LogOffDropdown from '../LogOffDropdown/LogOffDropdown';
import BookSearch from './BookSearch';
import categories from './categories';
import CategoryFilter from './CategoryFilter';
import {Box, Typography} from '@material-ui/core';


const MOBILE_WIDTH = 599;
const HIDE_THRESHOLD = 440;


export default function Header() {
  const classes = useStyles();
  const [windowWidth, setWindowWidth] = useState(0);
  const [stickyShown, showSticky] = useState(false);
  const [selectedCategoryId, selectCategory] = useState(null);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    setWindowWidth(window.innerWidth);
    window.onresize = () => {
      setWindowWidth(window.innerWidth);
    };

    const pageWrap = document.getElementsByTagName('html')[0];
    window.onscroll = function () {
      if (pageWrap.scrollTop > HIDE_THRESHOLD) {
        showSticky(true);
      } else if (pageWrap.scrollTop < HIDE_THRESHOLD) {
        showSticky(false);
      }
    };
    return () => window.removeEventListener('scroll');
  }, []);

  const selectedCategoryItem = categories.find((category) => category.id === selectedCategoryId);

  return (
    < div className={classes.header_wrap} >
      < div className={classes.header}
        style={{backgroundColor: `${selectedCategoryItem ? selectedCategoryItem.background : ''}`}}>
        <HeaderContainer>
          {!stickyShown && <LogOffDropdown></LogOffDropdown>}
          {!stickyShown && <img src='/svg/logo_yellow.svg' className={classes.logo}></img>}
          {selectedCategoryId === null ? <img src='/svg/pattern.svg' className={classes.pattern_background}></img> : ''}
          {selectedCategoryId === null ? <HeaderImages categories={categories} /> : <img src={selectedCategoryItem.icon} className={classes.section_icon_screentop}></img>}
          <Typography variant='h2' className={classes.page_title}>
            {selectedCategoryId === null ? 'Nurture the love of reading' : selectedCategoryItem.name}
          </Typography>
          {windowWidth > MOBILE_WIDTH && stickyShown === false && selectedCategoryId === null ?
            <div className={classes.book_image}>
            </div> : ''}
          {windowWidth > MOBILE_WIDTH && !stickyShown && <BookSearch stickyShown={stickyShown} categorySelected={selectedCategoryId !== null} />}
        </HeaderContainer>
      </div >
      < div className={`${classes.sub_header} ${stickyShown ? classes.fixed : ''}`}
        style={{backgroundColor: `${windowWidth <= MOBILE_WIDTH && stickyShown && selectedCategoryItem ? selectedCategoryItem.background : ''}`}}>
        {stickyShown &&
          <div className={classes.fixed_top}
            style={{backgroundColor: `${selectedCategoryItem ? selectedCategoryItem.background : ''}`}}>
            <div className={classes.nav_elements}>
              <img src='/svg/logo_yellow.svg' className={classes.logo + ' ' + classes.fixed_logo}></img>
              <LogOffDropdown></LogOffDropdown>
            </div>
            <BookSearch stickyShown={stickyShown} categorySelected={selectedCategoryId !== null}></BookSearch>
          </div>
        }
        {windowWidth <= MOBILE_WIDTH && stickyShown === false ? <>
          <BookSearch stickyShown={stickyShown} categorySelected={selectedCategoryId !== null}></BookSearch>
          {selectedCategoryId === null ? <div className={classes.book_image}>
          </div> : ''}
        </> : ''}
        <CategoryFilter
          categories={categories}
          selectedCategoryId={selectedCategoryId}
          onCategoryItemClick={selectCategory} />
      </div >
    </div >
  );
};
