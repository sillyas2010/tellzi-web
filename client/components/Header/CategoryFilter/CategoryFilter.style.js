import {styled, makeStyles} from '@material-ui/core/styles';
import {Box} from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  category_filter_wrap: {
    display: 'flex',
    flexDirection: 'row',
    overflowX: 'auto',
    paddingRight: '90px',
    [theme.breakpoints.up('xs')]: {
      paddingLeft: '20px',
    },
    [theme.breakpoints.up('sm')]: {
      paddingLeft: '30px',
    },
    [theme.breakpoints.up('lg')]: {
      paddingLeft: '90px',
    },
    '&::-webkit-scrollbar': {
      display: 'none',
    },
  },
  category_card_wrap: {
    flex: '0 0 12.5%',
    borderRadius: '16px',
    position: 'relative',
    top: '-50px',
    [theme.breakpoints.up('xs')]: {
      minwidth: '104px',
      height: '154px',
      margin: '33px 5px',
    },
    [theme.breakpoints.up('sm')]: {
      minwidth: '130px',
      height: '164px',
      margin: '30px 10px',
    },
    [theme.breakpoints.up('lg')]: {
      minWidth: '190px',
      maxWidth: '190px',
      height: '128px',
      margin: '30px 14px',
    },
  },
  category_front_card: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '10px',
  },
  category_background_card: {
    position: 'relative',
    left: '4px',
    zIndex: '-1',
    backgroundColor: theme.palette.background.default,
    [theme.breakpoints.up('xs')]: {
      bottom: '142px',
    },
    [theme.breakpoints.up('sm')]: {
      bottom: '156px',
    },
    [theme.breakpoints.up('lg')]: {
      bottom: '120px',
    },
  },
  category_card: {
    borderRadius: '16px',
    [theme.breakpoints.up('xs')]: {
      width: '104px',
      height: '146px',
    },
    [theme.breakpoints.up('sm')]: {
      width: '130px',
      height: '160px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '190px',
      height: '124px',
    },
  },
  cat_icon_wrap: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    [theme.breakpoints.up('xs')]: {
      height: '102px',
    },
    [theme.breakpoints.up('sm')]: {
      height: '116px',
    },
    [theme.breakpoints.up('lg')]: {
      height: '80px',
    },
  },
  cat_icon: {
    height: '100%',
    width: 'auto',
    margin: '0 auto',
    [theme.breakpoints.up('xs')]: {
      maxWidth: '80%',
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: '118px',
    },
  },
  cat_title: {
    textTransform: 'capitalize',
    width: '100%',
    textAlign: 'center',
    fontFamily: 'Circular Std, sans-serif',
    fontSize: '14px',
    fontWeight: '500',
    margin: '5px 0',
  },
  arrows_wrap: {
    [theme.breakpoints.up('xs')]: {
      height: '45px',
    },
    [theme.breakpoints.up('sm')]: {
      height: '75px',
    },
  },
  chosen_cat_indicator: {
    width: '40px',
    height: '40px',
    transform: 'rotate(45deg)',
    margin: '0 auto',
    borderRadius: '10px',
    [theme.breakpoints.up('xs')]: {
      dispplay: 'none',
    },
    [theme.breakpoints.up('sm')]: {
      dispplay: 'block',
    },
  },
  category_picked: {
    backgroundColor: 'white !important',
  },
  close: {
    backgroundImage: 'url(/svg/close.svg)',
    height: '13px',
    width: '13px',
    position: 'absolute',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    right: '15px',
  },
}));

