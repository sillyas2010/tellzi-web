import React, {useEffect, useState} from 'react';
import {useStyles} from './CategoryFilter.style';
import {Box, Typography} from '@material-ui/core';
import PropTypes from 'prop-types';

const MOBILE_WIDTH = 599;

export default function CategoryFilter(props) {
  const classes = useStyles();
  const [windowWidth, setWindowWidth] = useState(0);


  const unselect = (event) => {
    event.stopPropagation();
    props.onCategoryItemClick(null);
  };

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    window.onresize = () => {
      setWindowWidth(window.innerWidth);
    };
  }, []);

  return (
    <div className={classes.category_filter_wrap}>
      {props.categories.map((category) => {
        return (
          <div className={classes.category_card_wrap} key={category.id} onClick={() => props.onCategoryItemClick(category.id)}>
            <div className={classes.arrows_wrap}>
              {windowWidth > MOBILE_WIDTH && props.selectedCategoryId === category.id ?
                <div className={classes.chosen_cat_indicator} style={{backgroundColor: category.background}}>
                </div> : ''}
            </div>
            <div className={`
              ${classes.category_front_card} 
              ${classes.category_card} 
              ${props.selectedCategoryId === category.id ? classes.category_picked : ''}
            `} style={{backgroundColor: category.background}}>
              {props.selectedCategoryId === category.id && <div className={classes.close} onClick={unselect}></div>}
              <div className={classes.cat_icon_wrap}>
                <img src={category.icon} className={classes.cat_icon}></img>
              </div>
              <Typography variant='subtitle1' className={classes.cat_title}>
                {category.name}
              </Typography>
            </div>
            <div className={classes.category_background_card + ' ' + classes.category_card}
              style={{backgroundColor: `${props.selectedCategoryId === category.id ? category.subBackground : ''}`}}
            >
            </div>
          </div>
        );
      })}
    </div >
  );
};

CategoryFilter.propTypes = {
  selectedCategoryId: PropTypes.string,
  onCategoryItemClick: PropTypes.func,
  categories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
  })),
};
