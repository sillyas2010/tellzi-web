const categories = [
  {
    id: 'friendship',
    name: 'Friendship',
    icon: '/svg/section_image_friendship.svg',
    background: '#F8E4FD',
    subBackground: '#dab4e4',
  },
  {
    id: 'animals',
    name: 'Animals',
    icon: '/svg/section_image_animals.svg',
    background: '#D1E0FF',
    subBackground: '#6291F2',
  },
  {
    id: 'nature',
    name: 'Nature',
    icon: '/svg/section_image_nature.svg',
    background: '#D9F1D6',
    subBackground: '#56ae4c',
  },
  {
    id: 'dreams',
    name: 'Dreams',
    icon: '/svg/section_image_dreams.svg',
    background: '#F5E9BE',
    subBackground: '#fff384',
  },
  {
    id: 'folktales',
    name: 'Folktales',
    icon: '/svg/section_image_folktales.svg',
    background: '#FFDCB6',
    subBackground: '#E98010',
  },
  {
    id: 'creativity',
    name: 'Creativity',
    icon: '/svg/section_image_creativity.svg',
    background: '#DCEDFF',
    subBackground: '#94c9ff',
  },
  {
    id: 'stem',
    name: 'STEM',
    icon: '/svg/section_image_stem.svg',
    background: '#D9F1D6',
    subBackground: '#ABF1A2',
  },
  {
    id: 'family',
    name: 'Family',
    icon: '/svg/section_image_family.svg',
    background: '#ffb6a5',
    subBackground: '#ED694A',
  },
];

export default categories;
