const aListedLanguages = [
  {
    id: 'English',
    name: 'English',
  },
  {
    id: 'Spanish',
    name: 'Spanish',
  },
  {
    id: 'Guarani',
    name: 'Guarani',
  },
  {
    id: 'Quechua',
    name: 'Quechua',
  },
];

export default aListedLanguages;
