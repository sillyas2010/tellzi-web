import {styled, makeStyles} from '@material-ui/core/styles';
import {Container} from '@material-ui/core';
import theme from '@utils/theme';

export const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    '& .MuiInputLabel-formControl': {
      top: '-5px',
    },
    '& .MuiInputLabel-filled.MuiInputLabel-shrink': {
      transform: 'translate(12px, 10px) scale(0)',
    },
    '& .MuiFilledInput-input': {
      paddingTop: '10px',
      backgroundColor: 'transparent !important',
    },
    '& .MuiSelect-select:focus': {
      backgroundColor: '#F6F6F0',
    },
  },

  dropdownStyle: {
    borderRadius: '16px',
    backgroundColor: theme.palette.background.default,
    '& .MuiListItem-root.Mui-selected': {
      backgroundColor: 'transparent',
      backgroundImage: 'url(/svg/yellow_tick.svg)',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center right 14px',
      color: theme.palette.text.yellow,
    },
    '& .MuiListItem-root:hover': {
      backgroundColor: theme.palette.background.paper,
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },

  book_filter_wrap: {
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.up('lg')]: {
      display: 'block',
    },
  },
  dropdownFilter: {
    zIndex: '5',
    borderRadius: '16px',
    color: '#949490',
    fontFamily: 'Circular Std, sans-serif',
    fontWeight: '500',
    fontSize: '28px',
    height: '78px',
    width: '174px',
    paddingTop: '0',
    paddingBottom: '0',
  },
  inputLabel: {
    zIndex: '6',
  },
  filter_placeholder: {
    fontFamily: 'Circular Std, sans-serif',
    fontWeight: '500',
    fontSize: '28px',
  },
}));
