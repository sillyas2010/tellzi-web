import React from 'react';
import {useStyles} from './BookSearchFilter.style';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuList from '@material-ui/core/MenuList';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import aListedLevels from './aListedLeves';
import {Box, Typography} from '@material-ui/core';

export default function BookSearchFilter(props) {
  const classes = useStyles();
  const [option, setOption] = React.useState('');


  const handleChange = (event) => {
    setOption(event.target.value);
  };


  return (
    <div className={classes.book_filter_wrap} style={{zIndex: '10'}}>
      <FormControl variant="filled" className={classes.formControl}>
        <InputLabel id="select-filled-label" className={classes.inputLabel}>
          <Typography variant='subtitle1' className={classes.filter_placeholder}>
            Any level
          </Typography>
        </InputLabel>
        <Select
          disableUnderline
          labelId="select-filled-label"
          id="select-filled"
          value={option}
          onChange={handleChange}
          MenuProps={{
            getContentAnchorEl: null,
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            classes: {paper: classes.dropdownStyle},
          }}
          className={classes.dropdownFilter}
          style={{backgroundColor: `${props.categorySelected ? 'white' : '#F6F6F0'}`}}
        >
          {props.options.map((option) => {
            return (
              <MenuItem value={option.id} key={option.id}>{option.name}</MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
}
