const aListedLevels = [
  {
    id: 'level_any',
    name: 'Any Level',
  },
  {
    id: 'level_1',
    name: 'Level 1',
  },
  {
    id: 'level_2',
    name: 'Level 2',
  },
  {
    id: 'level_3',
    name: 'Level 3',
  },
  {
    id: 'level_4',
    name: 'Level 4',
  },
  {
    id: 'level_5',
    name: 'Level 5',
  },
  {
    id: 'level_6',
    name: 'Level 6',
  },
  {
    id: 'level_7',
    name: 'Level 7',
  },
];

export default aListedLevels;
