import {styled, makeStyles} from '@material-ui/core/styles';
import {Container} from '@material-ui/core';
import theme from '@utils/theme';

export const useStyles = makeStyles((theme) => ({
  book_search_wrap: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    [theme.breakpoints.up('xs')]: {
      bottom: '0px',
    },
    [theme.breakpoints.up('sm')]: {
      bottom: '120px',
      paddingLeft: '3.5%',
    },
    [theme.breakpoints.up('lg')]: {
      paddingLeft: '0',
      bottom: '185px',
    },
  },
  fixed: {
    paddingLeft: '0',
    [theme.breakpoints.up('xs')]: {
      bottom: '25px',
      width: '100%',
      marginLeft: '0',
    },
    [theme.breakpoints.up('sm')]: {
      bottom: '50px',
    },
  },
  root: {
    [theme.breakpoints.up('xs')]: {
      width: '100%',
    },
    [theme.breakpoints.up('sm')]: {
      width: '55%',
    },
    [theme.breakpoints.up('lg')]: {
      width: 'fit-content',
    },
    '& .MuiTextField-root': {
      [theme.breakpoints.up('xs')]: {
        margin: '8px 20px',
      },
      [theme.breakpoints.up('sm')]: {
        margin: theme.spacing(1),
      },
    },
    '& .MuiInputBase-root': {
      backgroundColor: 'transparent',
      [theme.breakpoints.up('xs')]: {
        paddingLeft: '60px',
      },
      [theme.breakpoints.up('sm')]: {
        paddingLeft: '81px',
      },
    },
    '& .MuiFilledInput-underline:before': {
      borderBottom: 'none',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottom: 'none',
    },
    '& .MuiInputBase-input': {
      fontFamily: 'Circular Std, sans-serif',
      fontWeight: '500',
      fontSize: '28px',
    },
    '& .MuiFilledInput-input': {
      [theme.breakpoints.up('xs')]: {
        padding: '14px 0px 10px',
      },
      [theme.breakpoints.up('sm')]: {
        padding: '22px 12px 10px',
      },
    },
  },
  searchField: {
    backgroundImage: 'url(/svg/search.svg)',
    backgroundRepeat: 'no-repeat',
    zIndex: '5',
    fontFamily: 'Circular Std, sans-serif',
    fontWeight: '500',
    fontSize: '28px',
    paddingTop: '0',
    paddingBottom: '0',
    [theme.breakpoints.up('xs')]: {
      width: '90%',
      maxWidth: '343px',
      height: '56px',
      backgroundColor: theme.palette.background.default,
      borderRadius: '100px',
      backgroundPosition: 'center left 12px',
      color: '#3232323d',
    },
    [theme.breakpoints.up('sm')]: {
      color: '#3232327a',
      backgroundPosition: 'center left 29px',
      borderRadius: '16px',
      width: '100%',
      maxWidth: '370px',
      height: '78px',
      backgroundColor: theme.palette.background.paper,
      marginLeft: '0px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '370px',
    },
  },
  fixed_searcfield: {
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0px !important',
      width: '95%',
    },
  },
}));
