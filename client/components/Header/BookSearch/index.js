import React from 'react';
import {useStyles} from './BookSearch.style';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuList from '@material-ui/core/MenuList';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import BookSearchLevelFilter from './BookSearchFilter/BookSearchFilter';
import aListedLevels from './BookSearchFilter/aListedLeves';
import aListedLanguages from './BookSearchFilter/aListedLanguages';


export default function BookSearch(props) {
  const classes = useStyles();

  return (
    <div className={`${classes.book_search_wrap} ${props.stickyShown ? classes.fixed : ''}`}>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="filled-basic"
          variant="filled"
          placeholder='Explore books...'
          className={`${classes.searchField} ${props.stickyShown ? classes.fixed_searcfield : ''}`}
          style={{backgroundColor: `${props.categorySelected ? 'white' : ''}`}} />
      </form>
      <BookSearchLevelFilter options={aListedLevels} categorySelected={props.categorySelected}></BookSearchLevelFilter>
      <BookSearchLevelFilter options={aListedLanguages} categorySelected={props.categorySelected}></BookSearchLevelFilter>
    </div>
  );
}

