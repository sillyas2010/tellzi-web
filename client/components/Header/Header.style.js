import {styled, makeStyles} from '@material-ui/core/styles';
import {Container} from '@material-ui/core';
import theme from '@utils/theme';

export const useStyles = makeStyles((theme) => ({
  'header_wrap': {
    width: '100%',
    height: '920px',
  },
  'header': {
    position: 'relative',
    zIndex: '2',
    background: theme.palette.background.yellow,
    [theme.breakpoints.up('xs')]: {
      overflow: 'visible',
      height: '660px',
    },
    [theme.breakpoints.up('sm')]: {
      overflow: 'hidden',
      borderRadius: '0 0 85px 85px',
    },
  },
  'page_title': {
    position: 'absolute',
    top: '440px',
    fontWeight: '500',
    [theme.breakpoints.up('xs')]: {
      width: '220px',
      fontFamily: 'Poppins, sans-serif',
      fontSize: '48px',
    },
    [theme.breakpoints.up('sm')]: {
      top: '350px',
      width: '47%',
      maxWidth: '400px',
      paddingLeft: '5%',
      fontFamily: 'Bree Serif',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '64px',
      maxWidth: '530px',
      paddingLeft: '0',
    },
  },
  'logo': {
    zIndex: '2',
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      position: 'absolute',
      transform: 'rotate(5.21deg)',
      left: '7%',
      bottom: '310px',
    },
    [theme.breakpoints.up('lg')]: {
      left: 'auto',
      bottom: 'auto',
      position: 'relative',
      alignSelf: 'flex-start',
    },
  },

  'book_image': {
    position: 'absolute',
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    zIndex: '2',
    [theme.breakpoints.up('xs')]: {
      top: '-15px',
      width: '323px',
      height: '35px',
      right: '-50px',
      backgroundImage: 'url(/svg/book_mobile.svg)',
    },
    [theme.breakpoints.up('sm')]: {
      top: 'auto',
      right: '0px',
      bottom: '0px',
      width: '900px',
      height: '340px',
      backgroundImage: 'url(/svg/book_desktop.svg)',
    },
    [theme.breakpoints.up('lg')]: {
      bottom: '-30px',
      width: '1500px',
      height: '678px',
    },
  },

  'pattern_background': {
    position: 'absolute',
    zIndex: '1',
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      width: 'auto',
      height: '85%',
      right: '-520px',
      top: '105px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1080px',
      height: 'auto',
      right: '0px',
      top: '0px',
    },
  },
  'sub_header': {
    width: '100%',
    position: 'relative',
    [theme.breakpoints.up('xs')]: {
      zIndex: '2',
      backgroundColor: '#E9E9E2',
      borderRadius: '24px',
      top: '-20px',
      paddingTop: '60px',
    },
    [theme.breakpoints.up('sm')]: {
      zIndex: '1',
      backgroundColor: theme.palette.background.paper,
      top: '-145px',
      borderRadius: '0px 0px 0px 30px',
      paddingBottom: '30px',
      paddingTop: '140px',
    },
  },
  'fixed': {
    zIndex: '2',
    position: 'fixed',
    paddingTop: '0',
    top: '0px !important',
  },
  'fixed_top': {
    borderRadius: '0 0 85px 85px',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingLeft: '7%',
    [theme.breakpoints.up('xs')]: {
      paddingTop: '30px',
      backgroundColor: '#E9E9E2',
      height: '125px',
    },
    [theme.breakpoints.up('sm')]: {
      height: '250px',
      backgroundColor: theme.palette.background.default,
    },
    [theme.breakpoints.up('lg')]: {
      paddingTop: '10px',
    },
  },
  'nav_elements': {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  'fixed_logo': {
    left: 'auto',
    bottom: 'auto',
    position: 'relative',
    alignSelf: 'flex-start',
  },
  'section_icon_screentop': {
    position: 'relative',
    alignSelf: 'flex-end',
    margin: 'auto 0',
    [theme.breakpoints.up('xs')]: {
      height: 'auto',
      width: '100%',
      minWidth: '300px',
      maxWidth: '320px',
      maxHeight: '400px',
      top: '-100px',
    },
    [theme.breakpoints.up('sm')]: {
      top: '-65px',
      width: '65%',
      minWidth: '350px',
      maxWidth: '400px',
      maxHeight: '450px',
      minHeight: '400px',
    },
    [theme.breakpoints.up('lg')]: {
      top: 'auto',
      height: '60%',
      width: 'auto',
      minWidth: '400px',
      maxWidth: '550px',
      maxHeight: 'unset',
      minHeight: 'unset',
    },
  },
}));

export const HeaderContainer = styled(Container)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  padding: '15px 0 0 15px',
  overflow: 'hidden',
  [theme.breakpoints.up('xs')]: {
    height: '665px',
  },
  [theme.breakpoints.up('sm')]: {
    height: '744px',
  },
  [theme.breakpoints.up('lg')]: {
    height: '804px',
  },
});


