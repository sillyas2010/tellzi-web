import CloseButton from '@components/CloseButton/CloseButton';


import useI18n from '@hooks/use-i18n';

import {Box, Button, SvgIcon, Typography, Grid, Slider} from '@material-ui/core';

import ArrowLeftIcon from '@public/svg/arrow-left.svg';
import GoodMarkIcon from '@public/svg/good-mark.svg';
import BadMarkIcon from '@public/svg/bad-mark.svg';
import TickIcon from '@public/svg/tick.svg';

import {bookShape} from '@utils/shapes';
import {fetchApi, generateDeviceId} from '@utils/api';

import {PropTypes} from 'prop-types';

import {useState} from 'react';

import {useStyles} from './BookReaderNav.style';

const BookReaderNav = ({
  currentPage, setPage, currentMark,
  setMark, onClose, book, isMarkPage,
  fontSizeMulti, changeFontSize,
}) => {
  const bookProgressPercent = Math.floor((currentPage + 1) / book.pages.length * 100),
    i18n = useI18n(),
    [isFontSizeOpened, toggleFontSizeSwitcher] = useState(false),
    classes = useStyles({bookProgressPercent, isFontSizeOpened}),
    openFontSizeSwitcher = () => toggleFontSizeSwitcher(true),
    closeFontSizeSwitcher = () => toggleFontSizeSwitcher(false),
    nextPage = () => setPage(currentPage+1),
    prevPage = () => setPage(currentPage-1 >= 0 ? currentPage-1 : 0),
    submitMark = async (mark) => {
      try {
        await fetchApi(`/stories/${book.id}/rating/`, {
          score: mark,
          uuid: generateDeviceId(),
        }, 'PUT');
      } catch (error) {
        console.error(error);
      }
      setMark(mark);
      setTimeout(() => {
        setPage(0);
        setMark(0);
        changeFontSize(null, 1);
        onClose();
      }, 1000);
    },
    goodMark = () => submitMark(5),
    badMark = () => submitMark(1);

  return (
    <>
      { !currentMark &&
        <Box className={classes.closeButtonWrapper}>
          <CloseButton onClick={onClose}/>
        </Box>
      }
      { isMarkPage ?
        /* Markbox */
        <Box className={classes.markBoxWrapper}>
          { !currentMark ? (
            <Box className={classes.marksWrapper}>
              <Typography variant="h3" component="h3" className={classes.markText}>
                {i18n.t('bookDetail.markBook')}
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Button className={classes.markButton} variant="contained" onClick={goodMark}>
                    <SvgIcon className={classes.markIcon} component={GoodMarkIcon} viewBox="0 0 140 140"/>
                  </Button>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Button className={classes.markButton} variant="contained" onClick={badMark}>
                    <SvgIcon className={classes.markIcon} component={BadMarkIcon} viewBox="0 0 140 140"/>
                  </Button>
                </Grid>
              </Grid>
            </Box>
          ) : (
            <Box>
              <SvgIcon component={TickIcon} viewBox="0 0 32 23"/>
              <Typography variant="h3" component="h4" className={classes.thankYouText}>
                {i18n.t('general.thankYou')}
              </Typography>
            </Box>
          )}
        </Box> :
        /* Navigation box */
        <>
          {/* Book progress */}
          <Box className={classes.bookProgressWrapper}>
            <Box className={classes.bookProgressRead}/>
            <Box className={classes.bookProgressLeft}/>
          </Box>
          {/* FontSize changer */}
          <Box className={classes.fontSizeSwitcherWrapper}>
            { !isFontSizeOpened ?
              <Button className={classes.fontSizeSwitcher} onClick={openFontSizeSwitcher}>
                <Typography variant="h3" component="h5" className={classes.fontSizeSwitcherText}>
                  {i18n.t('bookDetail.fontSizeChanger')}
                </Typography>
              </Button> :
              <Box className={classes.fontSizeSwitcherOpenedWrapper}>
                <Box className={classes.fontSizeSliderWrapper}>
                  <Typography className={classes.fontSizeChangerSmallText}>
                    {i18n.t('bookDetail.fontSizeChanger')}
                  </Typography>
                  <Slider value={fontSizeMulti} onChange={changeFontSize}
                    className={classes.fontSizeSwitcherSlider}
                    min={0.75}
                    step={0.1}
                    max={2.3}
                  />
                  <Typography className={classes.fontSizeChangerBigText}>
                    {i18n.t('bookDetail.fontSizeChanger')}
                  </Typography>
                </Box>
                <CloseButton onClick={closeFontSizeSwitcher}/>
              </Box>
            }
          </Box>
          {/* Page switcher */}
          <Box className={classes.pageSwitcherWrapper}>
            <Button className={classes.pageSwitcher} onClick={prevPage}>
              <SvgIcon className={classes.switcherIcon} component={ArrowLeftIcon} viewBox="0 0 64 64"/>
            </Button>
            <Button className={classes.pageSwitcher + ' ' + classes.pageSwitcherRight} onClick={nextPage}>
              <SvgIcon className={classes.switcherIcon} component={ArrowLeftIcon} viewBox="0 0 64 64"/>
            </Button>
          </Box>
        </>
      }
    </>
  );
};

BookReaderNav.propTypes = {
  book: bookShape,
  currentPage: PropTypes.number,
  setPage: PropTypes.func,
  currentMark: PropTypes.number,
  setMark: PropTypes.func,
  fontSizeMulti: PropTypes.number,
  changeFontSize: PropTypes.func,
  onClose: PropTypes.func,
  isMarkPage: PropTypes.bool,
};

export default BookReaderNav;
