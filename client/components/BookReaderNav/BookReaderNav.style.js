import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

const useStyles = makeStyles((theme) => ({
  closeButtonWrapper: {
    position: 'absolute',
    left: 16,
    top: 20,
    [theme.breakpoints.down('sm')]: {
      left: 15,
      top: 15,
    },
  },
  pageSwitcherWrapper: {
    position: 'absolute',
    right: 40,
    bottom: 37,
    [theme.breakpoints.down('sm')]: {
      display: ({isFontSizeOpened}) => isFontSizeOpened ? 'none': 'block',
    },
  },
  pageSwitcher: {
    padding: '1.4rem',
    borderRadius: '50%',
    minWidth: 0,
    backgroundColor: theme.palette.secondary.light,
    ['&:hover']: {
      backgroundColor: darken(theme.palette.secondary.light, .05),
    },
    ['& .MuiTouchRipple-child']: {
      backgroundColor: darken(theme.palette.secondary.light, .3),
    },
    [theme.breakpoints.down('sm')]: {
      padding: '1rem',
    },
  },
  pageSwitcherRight: {
    marginLeft: '1.5rem',
    backgroundColor: theme.palette.primary.main,
    ['& .MuiButton-label']: {
      transform: 'rotate(180deg)',
    },
    ['&:hover']: {
      backgroundColor: darken(theme.palette.primary.main, .05),
    },
    ['& .MuiTouchRipple-child']: {
      backgroundColor: darken(theme.palette.primary.main, .3),
    },
  },
  switcherIcon: {
    fontSize: '3.8rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
    },
  },
  bookProgressWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 5,
    [theme.breakpoints.down('sm')]: {
      display: ({isFontSizeOpened}) => isFontSizeOpened ? 'none': 'block',
      top: 'auto',
      bottom: 0,
    },
  },
  bookProgressRead: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    display: 'inline-block',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    width: ({bookProgressPercent}) => `${bookProgressPercent}%`,
    backgroundColor: theme.palette.primary.main,
    zIndex: 2,
  },
  bookProgressLeft: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'inline-block',
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.secondary.dark,
    zIndex: 1,
  },
  fontSizeSwitcherWrapper: {
    position: 'absolute',
    right: 40,
    top: 37,
    [theme.breakpoints.down('sm')]: {
      right: 'auto',
      top: 'auto',
      left: 0,
      bottom: 0,
    },
  },
  fontSizeSwitcher: {
    padding: '2.4rem 1.55rem 2.9rem',
    borderRadius: '50%',
    textTransform: 'capitalize',
    backgroundColor: theme.palette.secondary.light,
    ['&:hover']: {
      backgroundColor: darken(theme.palette.secondary.light, .05),
    },
    ['& .MuiTouchRipple-child']: {
      backgroundColor: darken(theme.palette.secondary.light, .3),
    },
    [theme.breakpoints.down('sm')]: {
      backgroundColor: theme.palette.primary.main,
      marginLeft: 40,
      marginBottom: 37,
      padding: '1.1rem .8rem 1.4rem',
    },
  },
  fontSizeSwitcherText: {
    lineHeight: '1.5rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.5rem',
    },
  },
  fontSizeSwitcherOpenedWrapper: {
    display: 'flex',
    minWidth: 446,
    maxWidth: '100%',
    borderRadius: 100,
    backgroundColor: theme.palette.primary.main,
    padding: '1.8rem 2.4rem',
    [theme.breakpoints.down('sm')]: {
      padding: '1.1rem .9rem',
      minWidth: 0,
      width: '100vw',
      borderRadius: 0,
    },
  },
  fontSizeSliderWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '80%',
    marginRight: 'auto',
  },
  fontSizeChangerSmallText: {
    marginRight: '.6rem',
    color: theme.palette.secondary.dark,
  },
  fontSizeChangerBigText: {
    marginLeft: '.6rem',
    fontSize: '2rem',
    color: theme.palette.secondary.dark,
  },
  fontSizeSwitcherSlider: {
    color: theme.palette.secondary.dark,
    ['& .MuiSlider-track, & .MuiSlider-thumb']: {
      color: theme.palette.text.primary,
    },
    ['& .MuiSlider-track, & .MuiSlider-rail']: {
      height: 4,
    },
    ['& .MuiSlider-thumb']: {
      width: 18,
      height: 18,
      marginTop: -7,
    },
  },
  markBoxWrapper: {
    height: '100%',
  },
  marksWrapper: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: 470,
    margin: '0 auto',
    [theme.breakpoints.down('sm')]: {
      maxWidth: 235,
      flexWrap: 'nowrap',
    },
  },
  markText: {
    textAlign: 'center',
    fontSize: '3.2rem',
    padding: '2rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '2.2rem',
      padding: '2rem 0',
    },
  },
  markButton: {
    width: '100%',
    padding: '2.7rem 0',
    backgroundColor: theme.palette.primary.main,
    ['&:hover']: {
      backgroundColor: darken(theme.palette.primary.main, .05),
    },
    ['& .MuiTouchRipple-child']: {
      backgroundColor: darken(theme.palette.primary.main, .3),
    },
  },
  markIcon: {
    fontSize: '8.75rem',
  },
  thankYouText: {
    marginTop: '.7rem',
    fontSize: '1.6rem',
  },
}));

export {
  useStyles,
};
