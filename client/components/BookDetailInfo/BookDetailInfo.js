import BookActions from '@components/BookActions/BookActions';

import Typography from '@material-ui/core/Typography';
import {Box, Hidden} from '@material-ui/core';

import {getBookAuthorsStr} from '@utils/index';
import {bookShape} from '@utils/shapes';

import PropTypes from 'prop-types';

import {useStyles} from './BookDetailInfo.style';

const BookDetailInfo = ({book, categories, onRead}) => {
  const classes = useStyles(),
    categoryNames = (categories.map((cat) => cat.name)).join(' · '),
    bookAuthors = getBookAuthorsStr(book, ' · ');

  return (
    <>
      <Hidden smDown>
        <Typography gutterBottom variant="h3" component="h1" className={classes.bookTitle}>
          {book.title}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p" className={classes.bookCategories}>
          {categoryNames}
        </Typography>
        <Box className={classes.bookActions}>
          <BookActions book={book}/>
        </Box>
      </Hidden>
      <Typography variant="h4" component="p" className={classes.bookDescription}>
        {book.synopsis}
      </Typography>
      <Hidden mdUp>
        <Box className={classes.bookMobileActions}>
          <BookActions onRead={onRead} book={book}/>
        </Box>
      </Hidden>
      <Typography variant="body2" color="textSecondary" component="p" className={classes.bookAuthors}>
        {bookAuthors}
      </Typography>
    </>
  );
};

BookDetailInfo.propTypes = {
  book: bookShape,
  categories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })),
};

export default BookDetailInfo;
