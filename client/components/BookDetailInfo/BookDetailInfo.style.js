import {makeStyles} from '@material-ui/core/styles';

const gridGutter = 16,
  useStyles = makeStyles((theme) => ({
    bookTitle: {
      fontSize: '3.2rem',
      lineHeight: '3.2rem',
      letterSpacing: '-.05rem',
      marginBottom: '.6rem',
    },
    bookCategories: {
      fontSize: '1.1rem',
      wordSpacing: '.1rem',
      fontWeight: 600,
    },
    bookActions: {
      margin: '2.6rem 0 3rem',
      maxWidth: '21rem',
    },
    bookMobileActions: {
      padding: `1.1rem ${gridGutter}px`,
      backgroundColor: theme.palette.common.white,
      boxShadow: '0 0 16px rgba(0, 0, 0, .06)',
      [theme.breakpoints.down('sm')]: {
        width: `calc(100% + ${gridGutter*6}px)`,
        margin: `0 0 .75rem -${gridGutter*2.5}px`,
      },
    },
    bookDescription: {
      fontSize: '1.25rem',
      lineHeight: '1.9rem',
      letterSpacing: '.02rem',
      marginBottom: '1.1rem',
    },
    bookAuthors: {
      fontSize: '.7rem',
      lineHeight: '1.2rem',
      fontWeight: 600,
      letterSpacing: '.02rem',
      marginBottom: '2rem',
    },
  }));

export {
  useStyles,
};
