import {makeStyles} from '@material-ui/core/styles';
import {darken} from '@material-ui/core/styles/colorManipulator';

const rawStyles = (theme) => ({
    listWrapper: {
      margin: '2rem 0',
    },
    listTitle: {
      fontWeight: 600,
      fontSize: '1.1rem',
      marginBottom: '1rem',
    },
    root: {
      minHeight: '100%',
      display: 'flex',
      flexDirection: 'column',
      borderRadius: 4,
    },
    mediaWrapper: {
      paddingTop: '110%',
    },
    mediaAction: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
    media: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      height: '100%',
    },
    bookContentWrapper: {
      flex: 1,
      display: 'flex',
    },
    bookContent: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    bookTitle: {
      flex: 1,
      fontSize: '1.2rem',
      marginBottom: '.4rem',
    },
    loadMoreWrapper: {
      textAlign: 'center',
      marginTop: '3rem',
    },
    loadMore: {
      padding: '1.1rem 3.2rem',
      display: 'inline-flex',
      borderRadius: 16,
      backgroundColor: theme.palette.primary.main,
      ['&:hover']: {
        backgroundColor: darken(theme.palette.primary.main, .05),
      },
      ['& .MuiTouchRipple-child']: {
        backgroundColor: darken(theme.palette.primary.main, .3),
      },
    },
    loadMoreIcon: {
      fontSize: '2.3rem',
      marginLeft: '-1.2rem',
    },
    loadMoreTitle: {
      fontFamily: theme.typography.h1.fontFamily,
      fontSize: '1.15rem',
      lineHeight: '3rem',
      textTransform: 'none',
      marginLeft: '2.5rem',
    },
  }),
  useStyles = makeStyles(rawStyles);

export {
  rawStyles,
  useStyles,
};
