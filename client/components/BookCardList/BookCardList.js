import {useState} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import {Box, Typography, Button, SvgIcon} from '@material-ui/core';
import BookCard from '@components/BookCard/BookCard';
import {useStyles, rawStyles} from './BookCardList.style';
import useI18n from '@hooks/use-i18n';
import LoadMoreIcon from '@public/svg/loadmore.svg';
import {useApi} from '@utils/api';

const bookCategoriesArr = (book, categories) => (book && book.categories && categories ?
    categories.results.filter((category) => ~book.categories.indexOf(category.id)) : []
  ),
  categoryNamesStr = (bookCategories) => (bookCategories.map((cat) => cat.name)).join(' · ');

const BookCardList = ({
  title, booksApi = {url: null, params: {}},
  limit: initLimit = 4, needsLoadMore = false,
  pageCount = 0, resAttr = 'results',
}) => {
  const classes = useStyles(),
    i18n = useI18n(),
    [currentLimit, setLimit] = useState(initLimit),
    {data: categories} = useApi('/categories/'),
    {data: books} = useApi(booksApi.url, {
      ...booksApi.params,
      limit: currentLimit,
    }, {stickyResult: true}),
    loadMore = () => {
      setLimit(currentLimit + pageCount);
    },
    bookList = books && books[resAttr] && books[resAttr].length ?
      books[resAttr].slice(0, currentLimit) :
      [];

  return (
    <Box className={classes.listWrapper}>
      <Typography className={classes.listTitle} variant="h5" component="h5">{title}</Typography>
      <Grid container spacing={3}>
        { !!bookList.length && !!categories &&
          bookList.map((book) => (
            <Grid key={`${book.title}-${book.id}`} item xs={6} md={3}>
              <BookCard
                href={`/books/${book.id}`}
                title={book.title}
                description={categoryNamesStr(bookCategoriesArr(book, categories))}
                image={book.cover}
                level={book.levels[0]}
                classes={rawStyles}
              />
            </Grid>
          ))
        }
      </Grid>
      { needsLoadMore && initLimit < bookList.length &&
        <Box className={classes.loadMoreWrapper}>
          <Button variant="contained" className={classes.loadMore} onClick={loadMore}>
            <SvgIcon
              className={classes.loadMoreIcon}
              component={LoadMoreIcon} viewBox="0 0 44 44"
            />
            <Typography variant="h6" component="h4" className={classes.loadMoreTitle}>
              {i18n.t('general.actionLoadMore')}
            </Typography>
          </Button>
        </Box>
      }
    </Box>
  );
};

BookCardList.propTypes = {
  books: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    cover: PropTypes.string,
    synopsis: PropTypes.string,
    levels: PropTypes.arrayOf(PropTypes.number),
  })),
  booksApi: PropTypes.shape({
    url: PropTypes.string,
    params: PropTypes.object,
  }),
  title: PropTypes.string,
  limit: PropTypes.number,
  resAttr: PropTypes.string,
  needsLoadMore: PropTypes.bool,
  pageCount: PropTypes.number,
};

export default BookCardList;

