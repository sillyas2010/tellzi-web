import React from 'react';
import {useStyles, HeaderImagesWrap} from './HeaderImages.style';

export default function HeaderImages(props) {
  const classes = useStyles();

  return (
    <HeaderImagesWrap>
      {props.categories.map((category) => {
        return (
          <div className={classes.header_image_wrap + ' ' + classes[category.id]} key={category.id}>
            <img src={category.icon} className={classes.header_image + ' ' + classes[category.id + '_image']}></img>
          </div>
        );
      })}
    </HeaderImagesWrap >
  );
};
