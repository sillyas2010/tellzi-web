import {styled, makeStyles} from '@material-ui/core/styles';
import {Box} from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  'header_image_wrap': {
    position: 'absolute',
    zIndex: '3',
  },

  'friendship': {
    zIndex: '1',
    [theme.breakpoints.up('xs')]: {
      right: '-104px',
      top: '105px',
      minWidth: '305px',
      maxWidth: '365px',
      width: '81%',
    },
    [theme.breakpoints.up('sm')]: {
      width: '450px',
      minWidth: 'unset',
      maxWidth: 'unset',
      top: 'auto',
      bottom: '-100px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '-95px',
      top: 'auto',
      bottom: '-130px',
      width: '606px',
    },
  },

  'animals': {
    [theme.breakpoints.up('xs')]: {
      minWidth: '150px',
      maxWidth: '200px',
      width: '40%',
      left: '10px',
      top: '22px',
    },
    [theme.breakpoints.up('sm')]: {
      left: 'auto',
      top: '-30px',
      right: '340px',
      width: '260px',
      minWidth: 'unset',
      maxWidth: 'unset',
    },
    [theme.breakpoints.up('lg')]: {
      top: '-1,84%',
      width: '318px',
      right: '676px',
    },
  },

  'nature': {
    [theme.breakpoints.up('xs')]: {
      minWidth: '150px',
      maxWidth: '200px',
      width: '40%',
      left: '60px',
      top: '240px',
    },
    [theme.breakpoints.up('sm')]: {
      left: 'auto',
      top: '130px',
      right: '210px',
      width: '220px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '487px',
      top: '144px',
      minWidth: 'unset',
      maxWidth: 'unset',
      width: '258px',
    },
  },

  'dreams': {
    [theme.breakpoints.up('xs')]: {
      left: '45%',
      top: '1%',
      minWidth: '130px',
      maxWidth: '165px',
      width: '35%',
    },
    [theme.breakpoints.up('sm')]: {
      left: 'auto',
      right: '100px',
      width: '210px',
      top: '25px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '363px',
      minWidth: 'unset',
      maxWidth: 'unset',
      width: '256px',
      top: '-22px',
    },
  },

  'folktales': {
    [theme.breakpoints.up('xs')]: {
      display: 'none',
    },
    [theme.breakpoints.up('lg')]: {
      transform: 'rotate(-13deg)',
      display: 'block',
      width: '113px',
      bottom: '319px',
      right: '214px',
    },
  },

  'creativity': {
    transform: 'rotate(-80deg)',
    [theme.breakpoints.up('xs')]: {
      right: '-40px',
      top: '470px',
      minWidth: '110px',
      maxWidth: '150px',
      width: '30%',
    },
    [theme.breakpoints.up('sm')]: {
      top: 'auto',
      bottom: '315px',
      right: '-60px',
      width: '180px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '-90px',
      minWidth: 'unset',
      maxWidth: 'unset',
      width: '220px',
      bottom: '280px',
    },
  },

  'stem': {
    transform: 'rotate(15deg)',
    [theme.breakpoints.up('xs')]: {
      right: '23px',
      top: '350px',
      minWidth: '60px',
      maxWidth: '95px',
      width: '16%',
    },
    [theme.breakpoints.up('sm')]: {
      top: 'auto',
      right: '90px',
      bottom: '300px',
    },
    [theme.breakpoints.up('lg')]: {
      right: '385px',
      bottom: '320px',
      minWidth: 'unset',
      maxWidth: 'unset',
      width: '100px',
    },
  },

  'family': {
    transform: 'rotate(-13deg)',
    [theme.breakpoints.up('xs')]: {
      top: '30px',
      right: '-11px',
      minWidth: '40px',
      maxWidth: '60px',
      width: '10%',
    },
    [theme.breakpoints.up('lg')]: {
      bottom: '467px',
      right: '145px',
      minWidth: 'unset',
      maxWidth: 'unset',
      width: '82px',
    },
  },

  'header_image': {
    width: '100%',
    height: 'auto',
  },

}));

export const HeaderImagesWrap = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  height: '650px',
});
