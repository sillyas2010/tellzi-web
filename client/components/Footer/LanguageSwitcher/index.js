import React from 'react';
import {useStyles} from './LanguageSwithcer.style';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import {Typography} from '@material-ui/core';

export default function LanguageSwithcer() {
  const classes = useStyles();
  const [option, setOption] = React.useState('');


  const handleChange = (event) => {
    setOption(event.target.value);
  };


  return (
    <FormControl variant="filled" className={classes.formControl} style={{zIndex: '10'}}>
      <InputLabel id="select-filled-label" className={classes.inputLabel} shrink={false}>
        <Typography variant='subtitle1' className={classes.languagePlaceholder}>
          English
        </Typography>
      </InputLabel>
      <Select
        disableUnderline
        labelId="select-filled-label"
        id="select-filled"
        value={option}
        onChange={handleChange}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          classes: {paper: classes.dropdownStyle},
        }}
        className={classes.languageDropdown}
        style={{backgroundColor: '#FFFFFF'}}
      >
        <MenuItem value={'English'} style={{backgroundColor: '#FFFFFF'}}>English</MenuItem>
        <MenuItem value={'Spanish'} style={{backgroundColor: '#FFFFFF'}}>Spanish</MenuItem>
      </Select>
    </FormControl>
  );
}
