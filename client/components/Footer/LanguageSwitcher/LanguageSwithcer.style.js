import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: 'fit-content',
    left: 'auto',
    marginLeft: '0px',
    '& .MuiInputLabel-formControl': {
      top: '-16px',
      paddingLeft: '8px',
    },
    '& .MuiFilledInput-input': {
      paddingTop: '10px',
      backgroundColor: 'transparent !important',
      color: 'transparent',
    },
    '& .MuiSelect-select:focus': {
      backgroundColor: '#F6F6F0',
    },
  },

  dropdownStyle: {
    borderRadius: '16px',
    backgroundColor: theme.palette.background.default,
    width: '174px',
    minWidth: 'unset !important',
    boxShadow: '0px 20px 40px rgba(0, 0, 0, 0.1)',
    '& .MuiListItem-root.Mui-selected': {
      backgroundColor: 'transparent',
    },
    '& .MuiListItem-root:hover': {
      backgroundColor: 'transparent',
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },

  languageDropdown: {
    zIndex: '5',
    borderRadius: '100px',
    height: '32px',
    width: '135px',
    paddingTop: '0',
    paddingBottom: '0',
  },
  inputLabel: {
    zIndex: '6',
  },
  languagePlaceholder: {
    fontFamily: 'Circular Std, sans-serif',
    fontWeight: '500',
    fontSize: '14px',
    color: theme.palette.text.primary,
  },
}));
