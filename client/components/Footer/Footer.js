import React from 'react';
import {useStyles, FooterMenu, SocialLink, FooterAdditional, AdditionalMenu, FooterContainer} from './Footer.style';
import {Box} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@components/Link/Link';
import LanguageSwithcer from './LanguageSwitcher';

export default function Footer() {
  const classes = useStyles();

  return (
    <div className={classes.footer}>
      <FooterContainer>
        <FooterAdditional>
          <div className={classes.FooterMainContent}>
            <Box display="flex" className={classes.actionsWrap}>
              <div className={classes.detailsWrap}>
                <LanguageSwithcer />
                <SocialLink>
                  <Link href="/" className={classes.socialLinkWrap}>
                    <img className={classes.socialLinkImg} src="/svg/in.svg" alt="" />
                  </Link>
                  <Link href="/" className={classes.socialLinkWrap}>
                    <img className={classes.socialLinkImg} src="/svg/fb.svg" alt="" />
                  </Link>
                </SocialLink>
              </div>
              <div className={classes.subscriptionWrap}>
                <TextField
                  id="filled-basic"
                  autoComplete="off"
                  variant="filled"
                  placeholder='Email'
                  className={classes.emailInput} />
                <Button variant="contained"
                  className={classes.subscribeButton}
                  style={{backgroundColor: '#CCA863'}}>Subscribe</Button>
              </div>
            </Box>
            <FooterMenu>
              <Link href="/" className={classes.footerMenuLink}>About us</Link>
              <Link href="/" className={classes.footerMenuLink}>Blog</Link>
              <Link href="/" className={classes.footerMenuLink}>Contact</Link>
            </FooterMenu>
          </div>
          <AdditionalMenu>
            <div className={classes.additionalRow}>
              <Link href="/" className={classes.footerAdditionalLink}>© 2020.</Link>
              <Link href="/" className={classes.footerAdditionalLink}>Legal</Link>
              <Link href="/" className={classes.footerAdditionalLink}>Privacy statement</Link>
            </div>
            <div className={classes.additionalRow}>
              <Link href="/" className={classes.footerAdditionalLink}>Accessibility</Link>
              <Link href="/" className={classes.footerAdditionalLink}>Site map</Link>
              <Link href="/" className={classes.footerAdditionalLink}>Cookie settings</Link>
            </div>
          </AdditionalMenu>
        </FooterAdditional>
      </FooterContainer>
    </div>
  );
};
