import {styled, makeStyles} from '@material-ui/core/styles';
import {Box, Container} from '@material-ui/core';
import theme from '@utils/theme';

export const useStyles = makeStyles((theme) => ({
  footer: {
    background: theme.palette.primary.main,
    overflow: 'hidden',
    [theme.breakpoints.up('xs')]: {
      padding: '35px 27px 18px 27px',
    },
    [theme.breakpoints.up('sm')]: {
      padding: '40px 0 18px 0',
    },
    [theme.breakpoints.up('lg')]: {
      padding: '57px 92px 18px 92px',
    },
  },
  FooterMainContent: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    [theme.breakpoints.up('xs')]: {
      flexDirection: 'column-reverse',
    },
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
    },
  },
  footerMenuLink: {
    textDecoration: 'none',
    fontWeight: '500',
    fontFamily: 'Circular Std, sans-serif',
    color: theme.palette.text.primary,
    [theme.breakpoints.up('xs')]: {
      fontSize: '15px',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '24px',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '40px',
    },
  },
  footerAdditionalLink: {
    margin: '0 5px',
    fontWeight: '500',
    fontFamily: 'Circular Std, sans-serif',
    fontSize: '14px',
    color: theme.palette.text.primary,
  },
  socialLinkWrap: {
    height: '34px',
    width: '34px',
  },
  socialLinkImg: {
    height: '100%',
    width: 'auto ',
    [theme.breakpoints.up('lg')]: {
      marginLeft: '20px',
    },
  },
  actionsWrap: {
    display: 'flex',
    flexDirection: 'column',
    width: 'fit-content',
  },
  detailsWrap: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  subscriptionWrap: {
    display: 'flex',
    [theme.breakpoints.up('xs')]: {
      marginTop: '10px',
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    [theme.breakpoints.up('lg')]: {
      marginTop: '20px',
    },
  },
  emailInput: {
    'fontFamily': 'Circular Std, sans-serif',
    'fontWeight': '500',
    'fontSize': '28px',
    'paddingTop': '0',
    'paddingBottom': '0',
    'borderRadius': '100px',
    'backgroundColor': theme.palette.background.default,
    'height': '32px',
    [theme.breakpoints.up('xs')]: {
      width: '100%',
    },
    [theme.breakpoints.up('sm')]: {
      width: 'auto',
    },
    '& .MuiInputBase-root': {
      backgroundColor: 'transparent',
    },
    '& .MuiFilledInput-underline:before': {
      borderBottom: 'none',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottom: 'none',
    },
    '& .MuiFilledInput-input': {
      paddingTop: '6px',
    },
  },
  subscribeButton: {
    marginLeft: '10px',
    borderRadius: '100px',
    height: '32px',
    paddingTop: '4px',
    maxWidth: '100px',
    [theme.breakpoints.up('xs')]: {
      marginTop: '5px',
    },
    [theme.breakpoints.up('sm')]: {
      marginTop: '0',
    },
  },
}));

export const FooterContainer = styled(Container)({
  display: 'flex',
  justifyContent: 'space-between',
});

export const FooterMenu = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  [theme.breakpoints.up('xs')]: {
    marginBottom: '33px',
  },
  [theme.breakpoints.up('sm')]: {
    minWidth: '180px',
    marginBottom: '0',
  },
  [theme.breakpoints.up('lg')]: {
    minWidth: '300px',
  },
});

export const SocialLink = styled(Box)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  height: '34px',
});

export const FooterAdditional = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  width: '100%',
});

export const AdditionalMenu = styled(Box)({
  display: 'flex',
  marginTop: '20px',
  [theme.breakpoints.up('xs')]: {
    flexDirection: 'column',
  },
  [theme.breakpoints.up('sm')]: {
    flexDirection: 'row',
  },
});
