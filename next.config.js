const {
    webpack: webpackWithDevtools,
  } = require('@zeit/next-source-maps')(),
  {clientSettings, serverSettings, getEnv} = require('./const/settings');

const nextConfig = (phase) => {
  const currentEnv = getEnv(phase),
    {isDev} = currentEnv;

  clientSettings.apiRoot = isDev ? clientSettings.devApiRoot : clientSettings.prodApiRoot;

  return {
    target: 'server',
    publicRuntimeConfig: {
      ...clientSettings,
      ...currentEnv,
    },
    serverRuntimeConfig: {
      ...serverSettings,
      ...currentEnv,
    },
    webpack: (config, props) => {
      if (isDev) {
        config = webpackWithDevtools(config, props);
        config.devtool = serverSettings.sourseMapDevTool;
      }
      config.module.rules.push({
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      });
      return config;
    },
  };
};

module.exports = nextConfig;
